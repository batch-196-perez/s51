import {useState} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';

export default function CourseCards({courseProp}){
	//console.log({courseProp}); //res: php-laravel(courseData[0])
	//console.log(typeof {courseProp}); //every component received information in a form of object
	
	//object destructuring
	const {name, description, price} = courseProp; //props.CourseProp

	// react hooks - useState -> store its state
	//Syntax:
		//const [getter, setter] = useState(initialGetterValue);
	const [count, setCount] = useState(0);
	const [seatCount, seatSetCount] = useState(10);
	console.log(useState(0));

	function enroll(){
		setCount(count + 1);
		seatSetCount(seatCount -1)
		if (count === 10 && seatCount === 0){
			setCount (count);
			seatSetCount(seatCount)
			alert("No more seats available. Check back later!");
		}
		//console.log(`Enrollesss: ${count}`);
	}
	
///Solution
/*	const [count, setCount] = useState(0);
	const [seats, seatSet] = useState(10);
	console.log(useState(0));

	function enroll (){
		if(seats > 0 ){
			setCount(count + 1);
			console.log(`Enrollees: ${count}`);
			setSeats(seats-1);
			console.log(`Seats: ${seats}`);
		} else {
			alert('No more seats available');
		}
	}*/


	return (
		<Card className ="cardCourse p-3 mb-5">
			<Card.Body>
				<Card.Title className="fw-bold">{name}</Card.Title>
				<Card.Subtitle>Course Description: </Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				<Card.Subtitle>Course Price:</Card.Subtitle>
				<Card.Text>{price}</Card.Text>
				<Card.Text>Enrollees: {count}</Card.Text>
				<Card.Text>Seats: {seatCount}</Card.Text>
				<Button variant="primary" onClick={enroll}>Enroll</Button>
			</Card.Body>
		</Card>
	)
}

// export default function CourseCards(){
// 	return (
// 		<Row className="mt-3 mb-3">
// 			<Col>
// 				<Card className ="cardCourse p-3">
// 					<Card.Body>
// 						<Card.Title>
// 							<h3>React JS</h3>
// 						</Card.Title>
// 						<Card.Text>
// 							<h6>Description: </h6>
// 							<p>Learn React JS</p>

// 							<h6>Price:</h6>
// 							<p>Php 40,000</p>
// 							<Button variant="primary">Enroll</Button>
// 						</Card.Text>
// 					</Card.Body>
// 				</Card>
// 			</Col>
// 		</Row>	
// 	)
// }