import {useState} from 'react';
import {Link} from 'react-router-dom';
import {Navbar, Container, Nav} from 'react-bootstrap';


export default function AppNavBar(){

	//state hook to store the user information stored in the login page
	//localStorage.getItem -- kunin value ng email from login

	const [user, setUser] = useState(localStorage.getItem("email"));
	console.log(user);

	return(

		<Navbar bg="light" expand="lg">
		    <Container>
		        <Navbar.Brand as={Link} to="/">Batch-196 Booking App</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="me-auto">
		          
		            <Nav.Link as={Link} to="/">Home</Nav.Link>
		            <Nav.Link as={Link} to="/courses">Courses</Nav.Link>
		          {
		          	(user !== null) ?
		          	<Nav.Link as={Link} to="/logout">Logout</Nav.Link>
		          	:
		          	<>
		            	<Nav.Link as={Link} to="/login">Login</Nav.Link>
		            	<Nav.Link as={Link} to="/register">Register</Nav.Link>
		            </>
		          }
		          </Nav>
		        </Navbar.Collapse>
		    </Container>
		</Navbar>
	)
};