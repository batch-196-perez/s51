import {Navigate} from 'react-router-dom';
import {useState, useEffect} from 'react';
import {Form, Button} from 'react-bootstrap';

export default function Login(){

	const[email, setEmail] = useState('');
	const[password, setPassword] = useState('');

	const [isActive, setIsActive]= useState(false);

	function authenticate(e) {

		e.preventDefault();

		//Set the email of the authenticated user in the local storage
		/*Syntax
			localStorage.setItem('propertyName/key',value);
		*/
		localStorage.setItem("email",email);

		setEmail('');
		setPassword('');

		alert('Successfully Login');
	};


	useEffect(() => {
		if (email !== '' && password !== ''){
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email,password]);
	return(
	// (email !== null ) ?
	// <Navigate to ="/courses"/>
	// :	
	<>
	<h1>Log In</h1>
	<Form onSubmit ={e => authenticate(e)}>
		<Form.Group controlId="userEmail">
			<Form.Label>Email Address</Form.Label>
			<Form.Control
				type="email"
				placeholder="Enter your email here"
				required
				value = {email}
				onChange = {e => setEmail(e.target.value)}
			/>
			<Form.Text className="text-muted">
				We'll never share your email with anyone else.
			</Form.Text>	
		</Form.Group>

		<Form.Group controlId="password1">
			<Form.Label>Password</Form.Label>
			<Form.Control
				type="password"
				placeholder="Enter your password here"
				required
				value = {password}
				onChange = {e => setPassword(e.target.value)}
			/>
		</Form.Group>
		{ isActive ? 
			<Button className="mt-3 mb-5" variant="success" type="submit" id="submitBtn">Log In
			</Button>

			:

			<Button className="mt-3 mb-5" variant="danger" type="submit" id="submitBtn" disabled>Log In
			</Button>

		}
	</Form>
	</>
	)
}