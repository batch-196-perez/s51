import {Container} from 'react-bootstrap';
import {BrowserRouter as Router, Route, Routes, Switch, Redirect} from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout from './pages/Logout'
import Register from './pages/Register';
import './App.css';

function App() {

    return (
      <>
        <Router>
          <AppNavbar/>
          <Container>
              <Routes>
                  <Route exact path="/" element={<Home/>}/>
                  <Route exact path="/courses" element={<Courses/>}/>
                  <Route exact path="/register" element={<Register/>}/>
                  <Route exact path="/login" element={<Login/>}/>
                  <Route exact path="/logout" element={<Logout/>}/>
                  <Route exact path="*" element={<Error/>}/>
              </Routes>
          </Container>
        </Router> 
      </>
      )
}

export default App;
